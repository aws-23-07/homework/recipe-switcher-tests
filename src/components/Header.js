import React from 'react';
import { Sun, Moon } from 'lucide-react';
import { useTheme } from '../context/ThemeContext';
import { Link } from 'react-router-dom';

const Header = () => {
  const { theme, toggleTheme } = useTheme();

  return (
    <header className="bg-gray-200 dark:bg-gray-800 p-4 flex justify-between items-center">
      <nav>
        <button
          onClick={toggleTheme}
          className="p-2 rounded-full bg-gray-300 dark:bg-gray-700"
        >
          {theme === 'light' ? <Sun className="text-yellow-500" /> : <Moon className="text-white" />}
        </button>
        <ul className="flex space-x-4">
          <li>
            <Link to="/">Recipes</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
