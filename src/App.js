import './App.css';
import { Route, Routes } from 'react-router-dom';
import { ThemeProvider } from './context/ThemeContext';

import RecipeList from './components/RecipeList';
import RecipeDetail from './components/RecipeDetail';
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  return (
    <ThemeProvider>
      <div className="min-h-screen flex flex-col">
        <Header />
        <Routes >
          <Route path="/" element={<RecipeList />} />
          <Route path="/:id" element={<RecipeDetail />} />
        </Routes>
        <Footer />
      </div>
      </ThemeProvider>

      );
}

      export default App;